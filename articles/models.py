from django.db.models import Model, TextField, CharField, BooleanField, CASCADE, ImageField, ForeignKey, IntegerField

class Article(Model):
    title = CharField(max_length=128)
    description = TextField()
    content = TextField()
    image = ImageField(upload_to = 'article-images')
    articles_count = IntegerField(default=0)

    def __str__(self):
        return self.title


