from django.shortcuts import render
from django.contrib.auth.models import User

from rest_framework.viewsets import ModelViewSet
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny

from articles.models import Article
from articles.serializers import ArticleSerializers, Userserializers


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = Userserializers

class ArticleViewSet(ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializers
    permission_classes = [AllowAny]
    parser_classes = [FormParser, MultiPartParser]




