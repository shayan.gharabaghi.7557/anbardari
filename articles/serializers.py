from django.contrib.auth.models import User

from rest_framework.serializers import ModelSerializer
from .models import Article

class Userserializers(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class ArticleSerializers(ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'

